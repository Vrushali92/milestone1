//
//  DetailViewController.swift
//  Milestone1
//
//  Created by Vrushali Kulkarni on 12/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

final class DetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    var selectedCountry: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnTap = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }

    func configureUI() {

        title = selectedCountry?.uppercased()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(suggest))

        if let selectedCountry = selectedCountry {
            imageView.image = UIImage(named: selectedCountry)
        }
    }

    @objc func suggest() {

        guard let image = imageView.image?.jpegData(compressionQuality: 0.8) else {
            print("No image found")
            return
        }
        let viewController = UIActivityViewController(activityItems: [image], applicationActivities: [])
        viewController.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(viewController, animated: true, completion: nil)
    }
}
