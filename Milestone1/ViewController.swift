//
//  ViewController.swift
//  Milestone1
//
//  Created by Vrushali Kulkarni on 12/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

final class FlagListViewController: UITableViewController {

    var countries = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        countries += ["estonia", "france", "germany", "ireland",
                      "italy", "nigeria", "poland", "monaco",
                      "russia", "spain", "uk", "us"]

        title = "Flags List"
    }
}

extension FlagListViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let countryIndex = countries[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "flagCell", for: indexPath)
        cell.imageView?.image = UIImage(named: countryIndex)
        cell.textLabel?.text = countryIndex.uppercased()
        return cell
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let viewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            viewController.selectedCountry = countries[indexPath.row]
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

